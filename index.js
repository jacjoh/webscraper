const puppeteer = require('puppeteer');
const chalk = require('chalk');
const fs = require('fs');

const error = chalk.bold.red;
const success = chalk.keyword("green");

class Program {    
    init(){
        (async () => {
            try {
                //Open the browser
                const browser = await puppeteer.launch({headless: true});
                //open a page
                const page = await browser.newPage();
                //Go to the imdb top chart and wait for it to load
                await page.goto('https://www.imdb.com/chart/top/',{waitUntil: 'domcontentloaded'});

                let movies = await page.evaluate(() => {
                    //scrape all the titles
                    const titles = Array.from(document.querySelectorAll('table tr .titleColumn a'))
                    .map(td => td.innerText);
                    //scrape all the ratings
                    const ratings = Array.from(document.querySelectorAll('table tr .imdbRating'))
                    .map(td => td.innerText);

                    let moviesList = []
                    //add the titles and ratings to a new list
                    for(let i = 0; i < titles.length && i < 10; i++)
                    {
                        moviesList[i] = {
                            title: titles[i],
                            rating: ratings[i]
                        };
                    }
                    return moviesList;
                });

                await page.close();
                await browser.close();

                //write the movies to a json file
                this.writeToFile(movies);
            }
            catch (err) {
                //Catch and display errors
                console.log(error(err));
                await browser.close();
                console.log(error("Browser Closed"));
            }
          })();
    }

    //Writes the movies to a json file
    writeToFile(movies) {
        fs.writeFile("top10.json", JSON.stringify(movies), function(err) {
            if (err) throw err;
            console.log(success("Saved!"));
        }); 
    }
}

//start program
new Program().init();